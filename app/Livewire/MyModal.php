<?php

namespace App\Livewire;

use Livewire\Component;

class MyModal extends Component
{
    public $isModalOpen = false;

    public function render()
    {
        return view('livewire.my-modal');
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }

    public function closeModal()
    {
        $this->isModalOpen = false;
    }

}