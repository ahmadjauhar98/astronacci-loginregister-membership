<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Article;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserAccessArticle extends Model
{
    use HasFactory;
    protected $table = "user_access_article";

    // RELATIONSHIP
    public function hasArticles(): HasMany
    {
        return $this->hasMany(Article::class, 'id', 'user_id');
    }
}
