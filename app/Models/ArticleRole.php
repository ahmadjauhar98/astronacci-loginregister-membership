<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleRole extends Model
{
    use HasFactory;

    protected $table = "articles_roles";

    // RELATIONSHIP
    public function articles(): HasMany
    {
        return $this->hasMany(Article::class, 'id', 'article_id');
    }

}
