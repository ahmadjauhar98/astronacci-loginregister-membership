<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use App\Models\UserAccessArticle;
use App\Models\Videos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class BaseController extends Controller
{
    public function __construct()
    {
        $this->roleGold = 3;
    }
    public function article()
    {
        $userId = Auth::id();
        $user = User::find($userId);
        $role_id = $user->role_id;

        $articles = DB::table('articles_roles as ar')
            ->join('article as a', 'ar.article_id', '=', 'a.id')
            ->select('a.*');
        if ($role_id != $this->roleGold) {
            $articles->where('ar.role_id', $role_id);
        }
        $articles = $articles->get();

        // dd($articles);

        return view('article/index', compact('articles'));
    }

    public function videos()
    {
        $userId = Auth::id();
        $user = User::find($userId);
        $role_id = $user->role_id;

        $videos = DB::table('videos_roles as vr')
            ->join('videos as v', 'vr.videos_id', '=', 'v.id')
            ->select('v.*');
        if ($role_id != $this->roleGold) {
            $videos->where('vr.role_id', $role_id);
        }
        $videos = $videos->get();

        return view('videos/index', compact('videos'));
    }
}