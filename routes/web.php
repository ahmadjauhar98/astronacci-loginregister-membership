<?php

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthSocialController;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard'); })->name('dashboard');

    Route::get('/article', [BaseController::class, 'article'])->name('article');

    Route::get('/videos', [BaseController::class, 'videos'])->name('videos');
});

// SOCIAL MEDIA LOGIN (GOOGLE)
Route::controller(AuthSocialController::class)->group(function () {
    // Route::get('auth/google', 'redirectToGoogle')->name('auth.google');
    Route::get('auth/redirect', function () {
        return Socialite::driver('google')->redirect();
    })->name('auth.google');

    Route::get('auth/google/callback', 'handleGoogleCallback');
});