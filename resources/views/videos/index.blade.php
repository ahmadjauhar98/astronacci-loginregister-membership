<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Videos') }}
        </h2>
    </x-slot>

    <div class="max-w-3xl mx-auto">
    <ul class="mt-5">
    @foreach ($videos as $data)
        <li class="border rounded-lg p-4 mb-4 hover:bg-gray-100 transition duration-300">
            <div class="flex items-center space-x-4">
                <div class="flex-shrink-0">
                    <img src="{{asset('img/thumbnail-img.png')}}" alt="Video Thumbnail" class="w-[15rem] h-[10rem] rounded-lg hover:cursor-pointer">
                </div>
                <div class="flex-1">
                    <h2 class="text-lg font-semibold">{{$data->title}}</h2>
                    <p class="text-gray-600">{{$data->description}}</p>
                </div>
            </div>
        </li>
    @endforeach
        <!-- Add more video items as needed -->
    </ul>
</div> 
</x-app-layout>
