<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Article') }}
        </h2>
    </x-slot>

    @foreach ($articles as $data)        
    @php
        $formattedDate = date('j F Y', strtotime($data->created_at));
    @endphp

        <div class="py-6">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <!-- item article  -->
                    <div class="bg-blue rounded-lg shadow-lg p-4">
                        <!-- Card Header -->
                        <div class="mb-4">
                            <h2 class="text-2xl font-semibold text-gray-800">{{$data->title}}</h2>
                            <p class="text-gray-500 text-sm">Published on {{$formattedDate}}</p>
                        </div>
                        <!-- Card Content -->
                        <div class="h-[50px] mb-1 overflow-hidden">
                            <p class="text-gray-700">
                                {{$data->content}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @include('article.js')    
</x-app-layout>
